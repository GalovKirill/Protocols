﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Lab23
{
    class Program
    {
        private const string Path = @"/home/galovkirill/Документы/8 Семестр/decryptAesEcb.txt";

        static void Main(string[] args)
        {
            try
            {
                var encrypted = Convert.FromBase64String(File.ReadAllText(Path));
                const string key = "YELLOW SUBMARINE";
                
                using (Aes myAes = Aes.Create())
                {
                    myAes.Padding = PaddingMode.Zeros;
                    myAes.Mode = CipherMode.ECB;
                    myAes.Key = Encoding.ASCII.GetBytes(key);
                    //myAes.IV = new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
                    ICryptoTransform decryptor = myAes.CreateDecryptor();
                    
                    using (var msDecrypt = new MemoryStream(encrypted))               
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))                     
                            using (var srDecrypt = new StreamReader(csDecrypt))                           
                                Console.WriteLine(srDecrypt.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }
    }
}