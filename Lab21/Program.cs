﻿using System;
using System.Linq;
using System.Text;
using ClassLibrary;

namespace Lab21
{
    class Program
    {
        static void Main(string[] args)
        {
            const string text = "Never trouble about trouble until trouble troubles you!";
            const string key = "ICE";
            byte[] decrpt = Encoding.ASCII.GetBytes(text);
            decrpt.XorRepeatKey(Encoding.ASCII.GetBytes(key));
            Console.WriteLine(BitConverter.ToString(decrpt).Replace("-", ""));
        }
    }
}