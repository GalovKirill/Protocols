﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary
{
    public static class Library
    {
        public static byte[] ConvertHexStrToByteArr(string str)
        {
            List<string> hex8bits = new List<string>(str.Length / 2 + 1);
            for (int i = 0; i < str.Length; i+=2)
            {
                hex8bits.Add(str.Substring(i,2));
            }
            List<byte> results = new List<byte>(str.Length / 2 + 1);
            
            foreach (var hexValue in hex8bits)
            {
                results.Add(Convert.ToByte(hexValue, 16));
            }
            return results.ToArray();
        }

        public static void Xor(this byte[] b1, byte[] b2)
        {
            for (int i = 0; i < b1.Length; i++)
            {
                b1[i] ^= b2[i];
            }
        }
        
        public static void XorOneSymbol(this byte[] b1, byte b2)
        {
            for (int i = 0; i < b1.Length; i++)
            {
                b1[i] ^= b2;
            }
        }

        public static void XorRepeatKey(this byte[] b, byte[] key)
        {
            for (int i = 0; i < b.Length; i+=key.Length)
            {
                for (int j = 0; j < key.Length && i + j < b.Length; j++)
                {
                    b[i + j] ^= key[j];
                }
            }
        }
        
        public static void XorRepeatKey(this byte[] b, List<byte> key)
        {
            for (int i = 0; i < b.Length; i+=key.Count)
            {
                for (int j = 0; j < key.Count && i + j < b.Length; j++)
                {
                    b[i + j] ^= key[j];
                }
            }
        }
        
        public static void THEDecode(byte[] b, ref bool isDecoded)
        {
            for (byte i = Byte.MinValue; i < byte.MaxValue; i++)
            {
                byte[] temp = new byte[b.Length];
                b.CopyTo(temp, 0);
                temp.XorOneSymbol(i);
                string str = Encoding.ASCII.GetString(temp);
                //Console.WriteLine(str);
                if (str.ToLower().Contains("the"))
                {
                    Console.WriteLine(str);
                    Console.WriteLine(i);
                    isDecoded = true;
                }
            }
        }

    }
}