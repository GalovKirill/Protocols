﻿using System;
using ClassLibrary;
namespace Task2
{
    using static Library;
    class Program
    {
        static void Main(string[] args)
        {
            const string s1 = "8f29336f5e9af0919634f474d248addaf89f6e1f533752f52de2dae0ec3185f818c0892fdc873a69";
            const string s2 = "bf7962a3c4e6313b134229e31c0219767ff59b88584a303010ab83650a3b1763e5b314c2f1e2f166";
            byte[] b1 = ConvertHexStrToByteArr(s1), b2 = ConvertHexStrToByteArr(s2);
            b1.Xor(b2);
            Console.WriteLine(BitConverter.ToString(b1));
        }
    }
}