﻿using System;
using System.Linq;
using System.Text;
using ClassLibrary;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            const string cipher = "191f1911160b0c580c101d581d0e1114583f1914191b0c111b583d1508110a1d56";
            var b = Library.ConvertHexStrToByteArr(cipher);
            for (byte i = byte.MinValue; i < byte.MaxValue; i++)
            {
                byte[] temp = new byte[b.Length];
                b.CopyTo(temp, 0);
                temp.XorOneSymbol(i);
                string str = Encoding.ASCII.GetString(temp);
                //Console.WriteLine(str);
                if (str.Contains("the"))
                {
                    Console.WriteLine(str);
                    Console.WriteLine(i);
                }
            }
        }
    }
}