﻿using System;
using System.IO;
using System.Linq;
using ClassLibrary;

namespace Lab24
{
    class Program
    {
        private const string Path = @"/home/galovkirill/Документы/8 Семестр/detectEcb.txt";
        static void Main(string[] args)
        {
            var lines = File.ReadAllLines(Path);
            const int hexBlockSize = 128 >> 2;
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                int k = 0;
                for (int j = 0; j < line.Length; j+= hexBlockSize)
                {


                    bool repeatedBlock = line.Remove(j, hexBlockSize).Contains(line.Substring(j, hexBlockSize));
                    if (repeatedBlock)
                        k++;
                    if (k > 1)
                    {
                        Console.WriteLine(i);
                        break;
                    }
                        
                    
                }
            }

        }
    }
}