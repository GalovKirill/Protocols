﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using ClassLibrary;
using Kaos.Combinatorics;

namespace Lab22
{
    class Program
    {
        private static readonly byte[] alphabet = Encoding.ASCII.GetBytes("abcdefghijklmnopqrstuvwxyz1234567890 ");

        static void Main(string[] args)
        {
            const string path = @"/home/galovkirill/Документы/8 Семестр/breakRepeatedKeyXor.txt";
            string text = File.ReadAllText(path);
            (byte Key, double Freq, int KeyLength) tuple = (0, double.MinValue, 0);
            for (int keyLength = 1; keyLength < 100; keyLength++)
            {
                string selectionSymbols = SelectionSymbols(text, keyLength);
                selectionSymbols.Select(c => c ^ byte.MaxValue);
                var bytes = Encoding.ASCII.GetBytes(selectionSymbols);
                (byte Key, double Frequence) minamlKey = SearchMinimalKey(bytes);
                Console.WriteLine(minamlKey.Key + " " + minamlKey.Frequence + " " + keyLength);
                if (minamlKey.Frequence > tuple.Freq)
                {
                    tuple = (minamlKey.Key, minamlKey.Frequence, keyLength);
                }
            }
            Console.WriteLine(tuple.KeyLength);
        }

        private static (byte, double) SearchMinimalKey(byte[] bytes)
        {
            (byte Key, double Frequence) minamlKey = (0, double.MinValue);
            byte[] copyBytes = new byte[bytes.Length];
            double lengthOfstr = copyBytes.Length;
            for (byte key = byte.MinValue; key < byte.MaxValue; key++)
            {
                bytes.CopyTo(copyBytes, 0);
                copyBytes.XorOneSymbol(key);
                double frequence = copyBytes.Count(alphabet.Contains) / lengthOfstr;
                if (frequence > minamlKey.Frequence)
                {
                    minamlKey = (key, frequence);
                }
            }
            return minamlKey;
        }

        private static string SelectionSymbols(string text, int i)
        {
            StringBuilder sb = new StringBuilder(text.Length / i);
            for (int j = 0; j * i < text.Length; j++)
            {
                sb.Append(text[j * i]);
            }
            return sb.ToString();
        }
    }
}