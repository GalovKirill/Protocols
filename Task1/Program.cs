﻿using System;
using ClassLibrary;


namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            //var str = "faea8766efd8b295a633908a3c0828b22640e1e9122c3c9cfb7b59b7cf3c9d448bf04d72cde3aaa0";
            string str = Console.ReadLine();
            var bytes = Library.ConvertHexStrToByteArr(str);
            var res = Convert.ToBase64String(bytes);
            Console.WriteLine(res);
        }
    }
}