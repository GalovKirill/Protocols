﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;

using ClassLibrary;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            const string path = @"/home/galovkirill/Документы/8 Семестр/detectSingleXor02";
            string[] lines = File.ReadAllLines(path);
            var bytess = lines.Select(Library.ConvertHexStrToByteArr);
            int i = 0;
            foreach (var b in bytess)
            {
                i++;
                bool isDecoded = false;
                Library.THEDecode(b, ref isDecoded);
                if (isDecoded)
                {
                    Console.WriteLine("Зашифрованная строка №" + i);
                }
            }
        }
    }
}